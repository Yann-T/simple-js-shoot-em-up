# Simple JavaScript Shoot'em Up minigame

This project was created in HTML, CSS and JavaScript using the p5.js library.
It's a simple shoot'em up type game playable in a browser

## References

[The p5.js website](https://p5js.org)
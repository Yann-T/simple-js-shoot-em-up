
function pythagore(x1,x2,y1,y2){
	var a = x1 - x2;
	var b = y1 - y2;
	var c = Math.sqrt( a*a + b*b );
	return c;
}

function getAngle(x,y,x2,y2){
	return Math.atan2(y2-y,x2-x);
}

class Ball{
	constructor(x,y,a,p,array){
		this.player = p
		this.x = x;
		this.y = y;
		this.size = 20;
		this.a = a;
		this.spd = 12;
		this.bullets = array
	}
	checkCollision(){
		if(this.player == true){
			for (var i = enemies.length - 1; i >= 0; i--) {
				if(enemies[i]){
					if(pythagore(this.x,enemies[i].pos.x,this.y,enemies[i].pos.y) < enemies[i].size/2+this.size/2){
						enemies[i].life--;
						if(enemies[i].life <= 0){
							score++;
							enemies.splice(i, 1);
						}
						var i = this.bullets.indexOf(this);
						this.bullets.splice(i,1);
						
					}
				}
			}
		}else{
			if(pythagore(this.x,player.pos.x,this.y,player.pos.y) < player.size/2+this.size/2){
				var i = this.bullets.indexOf(this);
				this.bullets.splice(i,1);
				player.life--;
			}
		}
		
	}
	display(){
		this.x = this.x+cos(this.a)*this.spd;
		this.y = this.y+sin(this.a)*this.spd;
		if(this.player){
			fill(0,220,255);
		}else{
			fill(255,80,80);
		}
		noStroke();
		ellipse(this.x,this.y,this.size,this.size);
		this.checkCollision();
	}
}

class Enemy{
	constructor(spd,cannon,reload,life,moveable){
		this.life = life;
		this.maxLife = life;
		this.size = 30;
		this.reload = reload;
		this.reloadMax = reload;
		this.moveable = moveable;
		var p = round(random(1,4));
		switch(p){
			case 1:
				this.pos = createVector(0,random(0,height));
				break;
			case 2:
				this.pos = createVector(width,random(0,height));
				break;
			case 3:
				this.pos = createVector(random(0,width),0);
				break;
			case 4:
				this.pos = createVector(random(0,width),height);
				break;
		}
		this.angle = 0;
		this.spd = spd;
		this.cannon = cannon;
		this.cannonBase = createVector(this.pos.x,this.pos.y);
		this.bullets = []
	}
	moveCannon(){
		push();
		this.cannonBase.x = this.pos.x;
		this.cannonBase.y = this.pos.y;
		translate(this.cannonBase.x,this.cannonBase.y)
		noStroke();
		rotate(getAngle(this.cannonBase.x,this.cannonBase.y,player.pos.x,player.pos.y))
		fill((255*this.life)/this.maxLife,0,0)
		rect(0,-5,this.size/1.5 ,10);
		pop();
	}
	shootCannon(){
		var a = getAngle(this.cannonBase.x,this.cannonBase.y,player.pos.x,player.pos.y)+random(-0.1,0.1);
		this.bullets.push(new Ball(this.cannonBase.x,this.cannonBase.y,a,false,this.bullets));
		if(this.bullets.length > 20){
			this.bullets = this.bullets.slice(-20);
		}

	}
	display(){
		
		if(this.moveable){
			this.move();
		}
		if(this.cannon){
			this.moveCannon();
			if(this.reload >0){
				this.reload--;
			}else{
				this.reload = this.reloadMax;
				this.shootCannon();
			}
			for (var i = this.bullets.length - 1; i >= 0; i--) {
				this.bullets[i].display()
			}
		}
		noStroke()
		fill((255*this.life)/this.maxLife,0,0);
		ellipse(this.pos.x,this.pos.y,this.size ,this.size );
		this.checkCollision();
	}
	move(){
		var a = getAngle(this.pos.x,this.pos.y,player.pos.x,player.pos.y);
		
		this.pos.x = this.pos.x+cos(a)*this.spd;
		if(this.pos.x < 0){
			this.pos.x = 0;
		}
		if(this.pos.x > width){
			this.pos.x = width;
		}
		this.pos.y = this.pos.y+sin(a)*this.spd;
		if(this.pos.y < 0){
			this.pos.y = 0;
		}
		if(this.pos.y > height){
			this.pos.y = height;
		}
	}
	checkCollision(){
		if(pythagore(this.pos.x,player.pos.x,this.pos.y,player.pos.y) < this.size/2 + player.size/2){
				var i = enemies.indexOf(this)
				enemies.splice(i,1);
				player.life--;
		}
	}
}

class Player{
	constructor(){
		this.accelX = 0;
		this.accelY = 0;
		this.life = 3;
		this.pos = createVector(width/2,height/2);
		this.angle = 0;
		this.size = 30;
		this.movingUP = false;
		this.movingDOWN = false;
		this.movingLEFT = false;
		this.movingRIGHT = false;
		this.spd = 8;
		this.cannonBase = createVector(this.pos.x,this.pos.y);
		this.bullets = []
	}
	moveCannon(){
		push();
		this.cannonBase.x = this.pos.x;
		this.cannonBase.y = this.pos.y;
		translate(this.cannonBase.x,this.cannonBase.y)
		noStroke();
		rotate(getAngle(this.cannonBase.x,this.cannonBase.y,mouseX,mouseY))
		fill(0,(150*this.life)/3,(255*this.life)/3)
		rect(0,-6,this.size/1.1,12);
		pop();
	}
	shootCannon(){
		var a = getAngle(this.cannonBase.x,this.cannonBase.y,mouseX,mouseY)+random(-0.08,0.08);
		this.bullets.push(new Ball(this.cannonBase.x,this.cannonBase.y,a,true,this.bullets));
		if(this.bullets.length > 15){
			this.bullets = this.bullets.slice(-15);
		}

	}
	display(){
		if(this.life > 0){
			this.move();
			this.moveCannon();
			for (var i = this.bullets.length - 1; i >= 0; i--) {
				if(this.bullets[i]){
					this.bullets[i].display()
				}
			}
			stroke(255);
			strokeWeight(3)
			fill(0,(150*this.life)/3,(255*this.life)/3)
			ellipse(this.pos.x,this.pos.y,this.size,this.size);
		}
	}
	move(){
		if(this.movingUP){
			this.pos.y -= this.spd;
		}
		if(this.movingDOWN){
			this.pos.y += this.spd;
		}
		if(this.movingLEFT){
			this.pos.x -= this.spd;
		}
		if(this.movingRIGHT){
			this.pos.x += this.spd;
		}
		if(this.pos.x < 0){
			this.pos.x = 0;
		}
		if(this.pos.x > width){
			this.pos.x = width;
		}
		if(this.pos.y < 0){
			this.pos.y = 0;
		}
		if(this.pos.y > height){
			this.pos.y = height;
		}
	}
}

var score = 0;
var player;
var enemies = [];
var walls = [];

function setup(){
	canvas = createCanvas(windowWidth,windowHeight);
	canvas.parent("container");
	player = new Player();
	//enemies.push(new Enemy(0.5,true,20,3,true))
}
function windowResized() {
	resizeCanvas(windowWidth, windowHeight);
}
function draw(){
	background(20);
	if (!player.life <=0) {
		for (var i = enemies.length - 1; i >= 0; i--) {
			enemies[i].display()
		}
		player.display();
	}else{
		fill(255);
		textSize(100);
		text("SCORE: "+score,width/2-250,height/2,1000)
	}

}

function keyPressed(){
	if(key == 'Z'){
		player.movingUP = true
	}
	if(key == 'Q'){
		player.movingLEFT = true
	}
	if(key == 'S'){
		player.movingDOWN = true
	}
	if(key == 'D'){
		player.movingRIGHT = true
	}
}
function keyReleased(){
	if(key == 'Z'){
		player.movingUP = false
	}
	if(key == 'Q'){
		player.movingLEFT = false
	}
	if(key == 'S'){
		player.movingDOWN = false
	}
	if(key == 'D'){
		player.movingRIGHT = false
	}
}
setInterval(function(){
	if(player.life >0){
		if(enemies.length > 50){
			enemies = enemies.splice(-50);
		}
		for (var i = enemies.length - 1; i >= 0; i--) {
			if(enemies[i].life <= 0){
				
			}
		}
		var t = false
		if(random(1,10) >7){ t = true;}
		enemies.push(new Enemy(random(0.5,8),t,random(15,60),random(1,5),true));
	}else{
		enemies = 0;
		console.log("score: "+score)
	}
},480)

setInterval(function(){
	if(mouseIsPressed){
		player.shootCannon();
	}
},150);
function mousePressed(){
	player.shootCannon();
}
function end(){

}